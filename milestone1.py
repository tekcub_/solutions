from pathlib import Path
import bs4

import numpy
import pandas
from pandas import DataFrame


def parse_sections(html):
    soup = bs4.BeautifulSoup(html,'html.parser')
    title = soup.title.text.strip()
    body = soup.body.text
    
    raw_bullets = soup.find_all('li')
    formatted_bullets = ''.join((
        '[{}]'.format(bullet.text.replace('\n','')) 
        for bullet in raw_bullets))
    
    return (title,body,formatted_bullets)


cleaned_pages = []
for path in Path('html_job_postings/').glob('*'):
    file = open(path)
    raw_page = file.read()
    row = parse_sections(raw_page)
    cleaned_pages.append(row)
    file.close()

df = DataFrame(cleaned_pages,columns=['title','body','bullets'])